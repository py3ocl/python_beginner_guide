# Python Beginner Guide

This project provides someone new to programming the ability to setup an environment on Windows for learning Python and some basic example of how to use Python.

# Download Python
Download and install [Python](https://www.python.org/downloads/)

You can see the download button on the page, for example:  
![Download Screenshot](images/download_screenshot.png)

# Install Python
Install Python, which was 3.11 at the time of producing this guide, by ticking "Add python.exe to PATH" and clicking "Customize installation".
![Install Screenshot](images/Python_installer_screenshot.png)

Click next on the first customize screen.
![Install Complete Screenshot](images/Python_installer_custom1_screenshot.png)

Tick the "Install Python for all users" and "Add Python to environment variables" boxes and click "Install" button.
![Install Complete Screenshot](images/Python_installer_custom2_screenshot.png)

Click "Close" on successful screen to complete installation.
![Install Complete Screenshot](images/Python_installer_completion_screenshot.png)

## Python tools
The default editor installed for Python is called IDLE, which allows users to write Python code and execute it.

## Using IDLE
When you first start IDLE, which you can do from the Windows menu by pressing the Windows key and typing IDLE, then selecting the IDLE application from the menu, you will initially see this application running.
![IDLE Start Screenshot](images/IDLE_start_screenshot.png)

Enter the following text `print("Hello World")` at the >>> prompt where the cursor is blinking.
![IDLE Start Screenshot](images/IDLE_hello1_screenshot.png)

Pressing enter after typing `print("Hello World")` will produce the following output:
![IDLE Start Screenshot](images/IDLE_hello2_screenshot.png)

## Running Python File
Download and save the `HelloWorld.py` file into a directory, which can be done by click the right mouse button on the `HelloWorld.py` file on the Python_Beginner_Guide page and using the "Save Link As" menu option.

Save the file in any folder location, such as my documents, but the example on the screen saves the file in `C:\python_beginner_guide`

From the file menu select open, navigate to the directory containing `HelloWorld.py` example and open the file.

A new window will open with the `HelloWorld.py` example loaded, and then you can select the Run menu and select Run Module.
![IDLE Hello Run Screenshot](images/IDLE_hello_run_screenshot.png)

## Installing Git Tools
Download and install the [Git Tools](https://git-scm.com/downloads) if you wish to download and use [Python Training](https://gitlab.com/py3ocl/training).

Click Windows link to go to download page:
![Git Download 1 Screen](images/Git_download1_screenshot.png)

Click on "Click here to download" and then run the installer once downloaded.
![Git Download 2 Screen](images/Git_download2_screenshot.png)

Run Git GUI from the start menu and click on "Clone Existing Repository"
![GitGUI Screen](images/GitGUI_screenshot.png)

Clone the training to copy the files from the Git server to your local computer.
Enter `https://gitlab.com/py3ocl/training` into the Source Location and browse for the folder you wish to store the training into, e.g. My Documents.
![GitGUI Clone Screen](images/GitGUI_clone_screenshot.png)
